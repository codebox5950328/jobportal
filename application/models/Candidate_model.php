<?php 

class Candidate_model extends CI_Model{

    public function add($candidate)
    {
        $this->db->insert("candidates",$candidate);
    }

    public function get_candidate($username,$password)
    {   
        $this->db->select('*');
        $this->db->from('candidates');
        $this->db->where('email_id',$username);
        $this->db->where('password',$password);
        $query=$this->db->get();   
        return $query->row_array();
    }


    public function get_candidate_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('candidates');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }    

  
    public function insert_job($job_applications)
    {
        $this->db->insert('job_applications',$job_applications);
    }

    
   
    public function applied_jobs($id)
    {
        $this->db->select ('*'); 
        $this->db->from ( 'jobs' );
        $this->db->join ( 'job_applications', 'job_applications.job_id = jobs.job_id');
        $this->db->join ( 'employers', 'employers.id = jobs.emp_id' );
        $this->db->where (array('candidate_id'=>$id));
        $query = $this->db->get();
        return $query->result_array ();
    }


    public function edit_profile($id)
    {
        $this->db->select('*');
        $this->db->from('candidates');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }

    public function update_profile($update,$candidate_id)
    {
        $this->db->where('id',$candidate_id);
        $this->db->update('candidates',$update);
    }
}
      
        
