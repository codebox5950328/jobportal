<?php

class Employer_model extends CI_Model{

    public function add($employer)
    {
        $this->db->insert("employers",$employer);
    }

    public function get_employer($username,$password)
    {    
        $this->db->select('*');
        $this->db->from('employers');
        $this->db->where('email_id',$username);
        $this->db->where('password',$password);   
        $query=$this->db->get();
        return $query->row_array();
    }
                   
    public function get_employer_by_id($id)
    {
        $this->db->select("*");
        $this->db->from('employers');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }
}