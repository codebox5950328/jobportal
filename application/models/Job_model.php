<?php

class Job_model extends CI_Model{

   public function search($job_type,$job_location)
   {
      if($job_location=="All Locations")
      {
         $this->db->select("*");
         $this->db->from("jobs");
         $this->db->where("job_type",$job_type);
         $this->db->join('employers','jobs.emp_id=employers.id');
         $query=$this->db->get();
         return $query->result_array();
      }
      else
      {
         $this->db->select("*");
         $this->db->from("jobs");
         $this->db->where("job_type",$job_type);
         $this->db->where("job_location",$job_location);
         $this->db->join('employers','jobs.emp_id=employers.id');
         $query=$this->db->get();
         return $query->result_array();
      }
   }

   public function add($job)
   {
         $this->db->insert("jobs",$job);
   }

   public function edit($id)
   {
     $this->db->select("*");
     $this->db->from("jobs");
     $this->db->where("job_id",$id);
     $query=$this->db->get();
     return $query->row_array();
   }


   public function update($data,$id)
   {
      $this->db->where('job_id',$id);
      $this->db->update('jobs',$data);
   }

   public function delete($id)
   {
      $this->db->delete('jobs',array('job_id'=>$id));
   }

   public function get_job_details($id)
   {
      $this->db->select("*");
      $this->db->from("jobs");
      $this->db->where("job_id",$id);
      $this->db->join('employers','jobs.emp_id=employers.id');
      $query=$this->db->get();
      return $query->row_array();
   }

   public function view_applications($id)
   {
      $this->db->select("*");
      $this->db->from("candidates");
      $this->db->join('job_applications','job_applications.candidate_id=candidates.id','right');
      $this->db->join('jobs','job_applications.job_id=jobs.job_id');
      $this->db->where(array('jobs.job_id'=>$id));
      $query=$this->db->get();
      return $query->result_array();
   }

   public function get_employer_jobs($emp_id)
   {
      $this->db->select("jobs.job_id,job_title,job_location,budget,count(candidate_id)");
      $this->db->from("jobs");
      $this->db->join('job_applications','job_applications.job_id=jobs.job_id');
      $this->db->where(array('job_applications.emp_id'=>$emp_id));
      $this->db->group_by('job_title');
      $this->db->having('count(candidate_id)>=',0);
      $query=$this->db->get();
      return $query->result_array();
   }
}