<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <h1 align="center"><u>Employer</u></h1>
    <?php include"login_header.php" ?>
    <main>
    <h2 align="center"><u>My Jobs</u></h2>
     <div class="table-container">
    <table class="table table-striped table-hover table-bordered" cellpadding="5" border="1" cellspacing="0">
        <tr class="table-secondary">
            <th>Job Title</th>
            <th>Job Location</th>
            <th>Max CTC Budget</th>
            <th>Applications</th>
            <th>Actions</th>
        </tr>
  <?php foreach($jobs as $job){ ?>
        <tr>
        <td><?php echo $job['job_title']; ?></td>
        <td><?php echo $job['job_location']; ?></td>
        <td><?php echo $job['budget']; ?></td>
        <td><a style="text-decoration:none" href="<?php echo site_url('job/view_applications/'.$job['job_id'])?>"><?php echo $job['count(candidate_id)'] ?></a></td>
        <td><a style="text-decoration:none" href="<?php echo site_url("job/update/".$job['job_id']) ?>">Edit</a> | <a style="text-decoration:none" href="<?php echo site_url('job/delete/'.$job['job_id']) ?>">Delete</a></td> 
        </tr>
        <?php }; ?> 
        </table> 
    </div>
        </main> 
        <?php include"login_footer.php"?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>     
    </body>
    </html>