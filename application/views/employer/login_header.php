<!DOCTYPE html>
<html>
    <head>
          <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/employer_style.css">
</head>
<body>
<nav class="navbar">
<ul class="navbar-list">
     <li class="navbar-items">
    <a class="nav-link" href="<?php echo site_url("employer/dashboard") ?>">Dashboard</a>
     </li>
     <li class="navbar-items">
       <a class="nav-link" href="<?php echo site_url("job/post"); ?>">Post New Job</a>
    </li>
    <li class="navbar-items">
    <a class="nav-link" href="<?php echo site_url("employer/my_jobs") ?>">My Jobs</a>
    </li>
    <li class="navbar-items">
     <a class="nav-link" href="<?php echo site_url("employer/profile") ?>">My Profile</a>
    </li>
     <li class="navbar-items">
     <a  class="nav-link" style="margin-left: 70vw" href="<?php echo site_url("employer/logout"); ?>">Logout</a>
    </li>
 </ul>
</nav>
</body>
</html>