<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <h1 style="text-align: center;"><u>Employer</u></h1>
<?php include 'login_header.php' ?>  
<main>
     <br><?php if ($this->session->flashdata('msg') != ''): 
    echo"<strong class=color>". $this->session->flashdata('msg')."</strong>"; 
    endif;?>
   <h2 class="dashboard-head" >Welcome to Job Portal Employer Dashboard</h2>
</main>
<?php include 'login_footer.php' ?>  
<a href="<?php echo site_url("jobportal"); ?>">Go back to jobportal</a>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>  
<body>
    </html>