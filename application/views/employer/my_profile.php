<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <h1 align="center"><u>Employer</u></h1>
   <?php include 'login_header.php' ?> 
   <h2 align="center"><u>My profile</u></h2> 
<main class="profile-main"> 
<table class="table table-striped table-hover table-bordered" cellpadding="20">
<tr>
  <th>Company Name:</th>
  <td><?php echo $employer['company_name']; ?></td>
</tr>
<tr>
  <th>Company Address:</th>
  <td><?php echo $employer['company_address']; ?></td>
</tr>
<tr>
  <th>First Name:</th>
  <td><?php echo $employer['first_name']; ?></td>
</tr>
<tr>
  <th>Last Name:</th>
  <td><?php echo $employer['last_name']; ?></td>
</tr>
<tr>
  <th>Gender:</th>
  <td><?php echo $employer['gender']; ?></td>
</tr>
<tr>
  <th>Contact Number:</th>
  <td><?php echo  $employer['contact_number']; ?></td>
</tr>
<tr>
  <th>Current Location:</th>
  <td><?php echo $employer['current_location']; ?></td>
</tr>
<tr>
  <th>Email ID:</th>
  <td><?php echo $employer['email_id']; ?></td>
</tr>
</table>
</main>
<?php include 'login_footer.php' ?> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>   
<body>