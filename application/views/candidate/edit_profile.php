<!DOCTYPE html>
<html>
    <head>
         <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/candidate_style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> 
</head>
<body>
<h1 align="center"><u>Candidate Update</u></h1>
<?php include 'login_header.php' ?>  
<main>
    <?php if ($this->session->flashdata('msg') != ''): 
    echo"<strong class=color>". $this->session->flashdata('msg')."</strong>"; 
     endif;?>
    
  <form  action="<?php echo site_url('candidate/edit_profile'); ?>" method="POST"  enctype="multipart/form-data">
     <div class="edit-form" >
    <div class="form-control">
        <label><b>Upload file:</b></label><br><input type="file" name="resume" value="<?php echo $resume; ?>">
         <br><?php echo form_error('resume', '<div class="error">', '</div>'); ?><br>   
    </div>
     <div class="form-control">
        <label><b>Experience:</b></label>
        <br><input type="radio" name="exp" value="Yes">Yes<span>&nbsp</span><input type="radio" name="exp" value="No">No
        <br><?php echo form_error('exp', '<div class="error">', '</div>'); ?><br>      
    </div>
    <div class="form-control">
        <label><b>Job Title</b></label><br><input type="text" name="candidate_title" value="<?php echo $candidate_title; ?>">
        <br><?php echo form_error('candidate_title', '<div class="error">', '</div>'); ?><br>         
    </div>
    <div class="form-control">
        <label><b>Current CTC</b></label><br><input type="text" name="current_ctc" value="<?php echo $current_ctc;?>">&nbsp<span>lakhs</span>
        <br><?php echo form_error('current_ctc', '<div class="error">', '</div>'); ?><br>         
    </div>
    <div class="form-control">
        <label><b>First Name</b><span style="color:red">*</span></label><br><input type="text" name="first_name" size="30" value="<?php echo $first_name;?>">
        <br><?php echo form_error('first_name', '<div class="error">', '</div>'); ?></br>   
    </div>
    <div class="form-control">
        <label><b>Last Name</b><span style="color:red">*</span><span>&nbsp</span><br><input type="text" name="last_name" size="30" value="<?php echo $last_name; ?>"></label>
        <br><?php echo form_error('last_name', '<div class="error">', '</div>'); ?></br>   
    </div>
    <div class="form-control">
        <label><b>Gender</b></label>
        <span>&nbsp</span><br><input type="radio" name="gender" value="male" <?php if($gender=="male"){ echo "checked";}?>>Male
        <input type="radio" name="gender" value="female" <?php if($gender=="female"){ echo "checked";}?>>Female
        <br><?php echo form_error('gender', '<div class="error">', '</div>'); ?></br> 
    </div> 
    <div class="form-control">
        <label><b>Date of Birth</b></label>
        <br><input type="date" name="date_of_birth" size="30" value="<?php echo $date_of_birth; ?>">
        <br><?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?></br>    
    </div>
    <div class="form-control">
        <label><b>Contact Number</b></label>
       <br><input type="number" name="contact_number" size="30" value="<?php echo $contact_number; ?>">
        <br><?php echo form_error('contact_number', '<div class="error">', '</div>'); ?><br>   
    </div>
    <div class="form-control">
        <label><b>Current Location</b></label>
        <span>&nbsp</span><br><input type="text" name="current_location" size="30" value="<?php echo $current_location; ?>">
        <br><?php echo form_error('current_location', '<div class="error">', '</div>'); ?></br> 
    </div>
    <div class="form-control">
        <label><b>Email ID</b><span style="color:red">*</span></label>
        <span>&nbsp</span><br><input type="text" name="email_id" size="30" value="<?php echo $email_id; ?>">
        <br><?php echo form_error('email_id', '<div class="error">', '</div>'); ?></br>    
    </div>
    <div class="form-control">
        <label><b>Password</b><span style="color:red">*</span></label>
        <span>&nbsp</span><br><input type="password" name="password" size="30" value="<?php echo $password; ?>">
        <br><?php echo form_error('password', '<div class="error">', '</div>'); ?></br>    
    </div> 
    <div>
        <input type="checkbox" name="terms" value="1">&nbspI Agree to the Terms and Conditions
        <span>&nbsp</span><br><?php echo form_error('terms', '<div class="error">', '</div>'); ?></br>   
    </div>   
    <div>
        <input type="submit" name="update" value="Update">
    </div>
    </div>                            
  </form>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<?php include 'login_footer.php' ?> 
<body>
</html>