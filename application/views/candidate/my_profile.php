<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> 
</head>
<body>
    <h1 style="text-align: center; margin-top:20px"><u>Candidate</u></h1>
    <?php include 'login_header.php' ?> 

   <h2 id="profile-head"><u>My profile</u></h2>
   <button id="edit-btn" ><a id="edit-link" href="<?php echo site_url('candidate/edit_profile') ?>">Edit</a></button> 

<main class="profile-main">
<table class="table table-striped table-hover table-bordered" cellpadding="20">
<tr>
  <th>Resume:</th>
  <td><a href="<?php echo site_url('candidate/resume_view/'.$candidate['id']); ?>"><?php echo $candidate['resume']; ?></a></td>
</tr>
<tr>
  <th>Experience:</th>
  <td><?php echo $candidate['experience']; ?></td>
</tr>
<tr>
  <th>Job Title:</th>
  <td><?php echo $candidate['candidate_title']; ?></td>
</tr>
<tr>
  <th>Current CTC:</th>
  <td><?php echo $candidate['current_ctc']; ?></td>
</tr>
<tr>
  <th>First Name:</th>
  <td><?php echo $candidate['first_name']; ?></td>
</tr>
<tr>
  <th>Last Name:</th>
  <td><?php echo $candidate['last_name']; ?></td>
</tr>
<tr>
  <th>Gender:</th>
  <td><?php echo $candidate['gender']; ?></td>
</tr>
<tr>
  <th>Date of Birth:</th>
  <td><?php echo $candidate['date_of_birth'] ; ?></td>
</tr>
<tr>
  <th>Contact Number:</th>
  <td><?php echo  $candidate['contact_number']; ?></td>
</tr>
<tr>
  <th>Current Location:</th>
  <td><?php echo $candidate['current_location']; ?></td>
</tr>
<tr>
  <th>Email ID:</th>
  <td><?php echo $candidate['email_id']; ?></td>
</tr>
</table>
</main>

<?php include 'login_footer.php' ?>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<body>
</html>   