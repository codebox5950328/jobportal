<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/candidate_style.css">
</head>
<body>
<nav class="navbar">
<ul class="navbar-list">
     <li class="navbar-items">
     <a  class="nav-link" href="#" aria-current="page">Home</a>
     </li>
     <li class="navbar-items">
      <a  class="nav-link" href="<?php echo site_url("candidate/login") ?>" >Log in</a>
    </li>
    <li class="navbar-items">
    <a class="nav-link" href="<?php echo site_url("candidate/register") ?>">Sign Up</a>
    </li>
    <li class="navbar-items">
    <a  class="nav-link" href="#" aria-current="page">Contact Us</a> 
    </li>
 </ul>
</nav>
</body>
</html>