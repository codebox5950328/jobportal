<!DOCTYPE html>
<html>
    <head>
       
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
          <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/candidate_style.css"> 
</head>
<h1 class="login-head" ><u>Candidate login</u></h1>
<?php include 'header.php' ?>  
<main>
    <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="<?php echo site_url("candidate/login"); ?>" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="username" id="username" class="form-control">
                                <?php  echo form_error('username', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="text" name="password" id="password" class="form-control">
                                <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Login">
                                <span>
                                <button class="btn btn-info btn-md" name="sign_up"><a class="signup-link" href="<?php echo site_url("candidate/register")?>">Sign Up</a></button>
                             </span>
                            </div>
                        </form>
                    </div>s
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>  
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<body>
    </html>