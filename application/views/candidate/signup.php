<!DOCTYPE html>
<html>
    <head>
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
       <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/candidateStyle.css"> 
</head>
<body>
 <h1 class="head-signup"><u>Candidate SignUp</u></h1>   
<?php include 'header.php' ?>  

<div id="signup">
        <div class="container">
            <div id="signup-row" style="width:67vw">
                <div id="signup-column" class="col-md-6">
                    <div id="signup-box" class="col-md-12">
                        <form id="signup-form" class="form" action="<?php echo site_url("candidate/register"); ?>" method="post">
                            <h3 class="text-center text-info">Signup</h3>
                            <div class="form-group">
                                <label for="candidate_title" class="text-info" >Job Title:</label><br>
                                <input type="text" name="candidate_title" id="candidate_title" class="form-control" value="<?php echo set_value('candidate_title'); ?>">
                                <?php  echo form_error('candidate_title', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="current_ctc" class="text-info">Current CTC:</label><br>
                                <input type="number" name="current_ctc" id="current_ctc" class="form-control" value="<?php echo set_value('current_ctc'); ?>">
                                <?php echo form_error('current_ctc', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="first_name" class="text-info">First Name:</label><br>
                                <input type="text" name="first_name" id="first_name" class="form-control"value="<?php echo set_value('first_name'); ?>">
                                <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="text-info">Last Name:</label><br>
                                <input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo set_value('last_name'); ?>">
                                <?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="gender" class="text-info">Gender:</label><br>
                                <input type="radio" name="gender" id="male" value="male" class="form-control-gender"><span>&nbspMale</span>
                                <input type="radio" name="gender" id="female" value="female" class="form-control-gender"><span>&nbspFemale</span>
                                <?php echo form_error('gender', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="date_of_birth" class="text-info">Date Of Birth:</label><br>
                                <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" value="<?php echo set_value('date_of_birth'); ?>">
                                <?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="contact_number" class="text-info">Contact Number:</label><br>
                                <input type="text" name="contact_number" id="contact_number" class="form-control" value="<?php echo set_value('contact_number'); ?>">
                                <?php echo form_error('contact_number', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="current_location" class="text-info">Current Location:</label><br>
                                <input type="text" name="current_location" id="current_location" class="form-control" value="<?php echo set_value('current_location'); ?>">
                                <?php echo form_error('current_location', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="email_id" class="text-info">Email ID:</label><br>
                                <input type="text" name="email_id" id="email_id" class="form-control" value="<?php echo set_value('email_id'); ?>">
                                <?php echo form_error('email_id', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="number" name="password" id="password" class="form-control" value="<?php echo set_value('password'); ?>">
                                <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="re-enter_password" class="text-info">Re-enter Password:</label><br>
                                <input type="text" name="re-enter_password" id="re-enter_password" class="form-control">
                                <?php echo form_error('re-enter_password', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="terms" value="1"><span>&nbspI Agree to the Terms and Conditions</span>
                                <?php echo form_error('terms', '<div class="error">', '</div>'); ?>   
                           </div>
                           <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Signup">
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?> 
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>
    </html>