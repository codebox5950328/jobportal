<!DOCTYPE html>
<html>
    <head>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> 
         <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/candidate_style.css">
</head>
<body>
<main class="main">
<h1 style="text-align: center; margin-top:20px;"><u>Candidate</u></h1>  
<?php include 'login_header.php' ?>
<h2 class="dashboard-head" >Welcome to Job Portal Candidate Dashboard</h2>
<?php include 'login_footer.php' ?>  
<a href="<?php echo site_url("jobportal"); ?>">Go back to jobportal</a>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>
    </html>




   