<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> 
</head>
<body>
    <h1 style="text-align: center; margin-top:20px"><u>Candidate</u></h1>
<?php include 'login_header.php' ?>  
<main style="text-align: center;">
    <h2 style="margin-top:20px; margin-bottom: 20px;">My Applied Jobs</h2>
 <div class="table-container">   
 <table class="table table-striped table-hover table-bordered"  cellpadding="7" border="1" cellspacing="0">
    <tr class="table-secondary">
        <th>Title</th>
        <th>Company Name</th>
        <th>Current Location</th>
        <th>Min. Experience Required</th>
        <th>Applied On</th>
    </tr>
    <?php foreach($jobs as $job){ ?>
    <tr>
        <td><?php echo $job['job_title'] ?></td>
        <td><?php echo $job['company_name'] ?></td>
        <td><?php echo $job['current_location'] ?></td>
        <td><?php echo $job['min_experience_required'] ?></td>
        <td><?php $present_time=time();
                   $applied_time=round($present_time-$job['applied_on']);
                   $seconds=round($applied_time%60);
                   $minutes = round($applied_time / 60) % 60;
                   $hours=round($applied_time%(60*60*24)/3600);
                   $days = round($applied_time/ (60 * 60 * 24));
                   if($days>0){
                   echo $days.'<span>&nbspdays ago</span>';
                      }
                  elseif($hours>0){
                  echo $hours. '<span>&nbsphours ago</span>';
                                         }
                  elseif($minutes>0){
                  echo $minutes. '<span>&nbspminutes ago</span>';
                        }
                  else{
                  echo $seconds.'<span>&nbspseconds ago</span>' ;
                          } ?></td>

    </tr>
    <?php };?> 
</table> 
</div>    
</main>
<?php include 'login_footer.php' ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>  
<body>
    </html>