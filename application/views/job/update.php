<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
         <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/job_style.css">
</head>
<body>
    <h2 align="center"><u>Edit Job</u></h2>
      <?php include APPPATH.'views/employer/login_header.php' ?>
    <main>
        <form action="<?php echo site_url('job/update/'.$job['job_id']); ?>" method="POST">
            <div class="job-form">
                <div class="form-control">
                <label><b>Job Title</b></label>
                <br><input type="text" name="job_title" value="<?php echo $job['job_title']; ?>">
                <?php echo form_error('job_title', '<div class="error">', '</div>'); ?>
            </div>
            <br><div class="form-control">
                <label><b>Description</b></label>
                 <br><textarea name="description" size="20"> <?php echo $job['description']; ?>></textarea>
                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
            </div>
            <br><div class="form-control">
                <label><b>Budget CTC</b></label>
                 <br><input type="text" name="budget_ctc" value="<?php echo $job['budget']; ?>" >&nbsp<span>lakhs</span>
                 <?php echo form_error('budget_ctc', '<div class="error">', '</div>'); ?>          
            </div>
            <br><div class="form-control">
                <label><b>Min.Experience Required</b></label>
                 <br><input type="number" name="experience_require" min="0" value="<?php echo $job['min_experience_required']; ?>">
                 <?php echo form_error('experience_require', '<div class="error">', '</div>'); ?>          
            </div>
            <div class="form-control"><span class="btn-span">
                 <label><b>Job Type</b></label>
                <select name="job_type">
                <option value="Full time">Full Time</option>
                <option value="Part time">Part Time</option>
                </select>
            </span>
            <span class="btn-span">
                <label><b>Job Location</b></label>
                <select name="job_location">
                <option value="Mumbai">Mumbai</option>
                <option value="Pune">Pune</option>
                <option value="Delhi">Delhi</option>
                <option value="Bangalore">Bangalore</option>
                </select>
            </span></div>
            <br><div id='update-btn'>
                <button  name="update" type="submit">Update</button>
            </div>   
       </div>
       </form>   
   </main>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <?php include APPPATH.'views/employer/login_footer.php' ?>  
</body>
    </html>
