<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
         <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/job_style.css">
</head>
<body>
    <h1 align="center"><u>Job</u></h1>
 <?php include APPPATH."views/candidate/login_header.php" ?>
  <h2 align="center" style="margin-top:20px"><u>Job Details</u></h2>

  <table class="table table-striped table-hover table-bordered " cellpadding="20" width="30%">
  <tr>
  <th>Job ID:</th>
  <td><?php echo $job['job_id'] ?></td>
  </tr>
  <tr>
  <th>Job Title:</th>
  <td><?php echo $job['job_title'] ?></td>
  </tr>
  <tr>
  <th>Job Description:</th>
  <td><?php echo $job['description'] ?></td>
  </tr>
  <tr>
  <th>Job Location:</th>
  <td><?php echo $job['job_location'] ?></td>
  </tr>
  <tr>
  <th>Company Name:</th>
  <td><?php echo  $job['company_name'] ?></td>
  </tr>
  <tr>
  <th>Budget:</th>
  <td><?php echo  $job['budget']."&nbsp"."lakhs" ?></td>
  </tr>
  <tr>
  <th>Min Experience required:</th>
  <td><?php echo $job['min_experience_required'] ?></td>
  </tr>
  <tr>
  <th></th>
  <td><button type="submit"><a href="<?php echo site_url('candidate/insert_job/'.$job['job_id']); ?>">Apply</a></button></td>
 </tr>
</table>
</main>
<?php include APPPATH."views/candidate/login_footer.php" ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>  
</body>
</html>