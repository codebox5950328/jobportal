<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> 
</head>
<body>
    <h1 style="margin-top:20px; text-align: center;"><u>Candidate</u></h1>
<?php include APPPATH.'views/candidate/login_header.php' ?>  
<main align="center">
    <h1 style="margin-top: 20px;">Job Search</h1>
<form action="<?php echo site_url("job/search"); ?>" method="POST">
<div class="search">   
<div class="search-items">
    <label><b>Job Type:</b></label>
    <select name="job_type">
    <option  value="select" selected>-select-</option>
    <option value="Full Time" <?php echo set_select('job_type', 'Full Time'); ?>>Full Time</option>
    <option value="Part Time" <?php echo set_select('job_type', 'Part Time'); ?>>Part Time</option>
    <option value="Contract Based" <?php echo set_select('job_type', 'Contract Based'); ?>>Contract Based</option>
    </select>
</div>
<div class="search-items">
    <label><b>Job Location:</b></label>
    <select name="job_location" >
    <option value="All Locations" selected>All Locations</option>
    <option value="Mumbai" <?php echo set_select('job_location', 'Mumbai'); ?>>Mumbai</option>
     <option value="Pune" <?php echo set_select('job_location', 'Pune'); ?>>Pune</option>
    <option value="Delhi">Delhi</option>
    <option value="Bangalore" <?php echo set_select('job_location', 'Bangalore'); ?>>Bangalore</option>
    </select>
</div>
<div class="search-items">
    <button type="submit">Search</button>
</div>
</div>    
</form>
<h2>Search Result</h2>
<div class="search-container">
<table class="table table-striped table-hover table-bordered" cellpadding="5" cellspacing="0">
    <tr class="table-secondary">
        <th>Job Title</th>
        <th>Company Name</th>
        <th>Job Location</th>
    </tr>
    <?php foreach($jobs as $job){ ?>
    <tr>
        <td><a style="text-decoration:none" href="<?php echo site_url("job/details/".$job['job_id']); ?>"><?php echo $job['job_title']; ?></a></td>
        <td><?php echo $job['company_name']; ?></td>
        <td><?php echo $job['job_location']; ?></td>
     </tr>   
    <?php };?> 
</table> 
</div>      
</main>
<?php include APPPATH.'views/candidate/login_footer.php' ?>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script> 
<body>
    </html>