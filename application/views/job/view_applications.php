<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
           <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/job_style.css">
</head>
<body>
    <h1 align="center"><u>Employer</u></h1>
    <?php include APPPATH.'views/employer/login_header.php' ?>
    <h2 align="center" style="margin-top: 20px;">All applications for <?php echo $applications[0]['job_title'] ?></h2>
    <main style="margin-top: 20px; margin-bottom:20px">
        <table class="table table-striped table-hover table-bordered" cellpadding="4" cellspacing="0" border="1">
            <tr>
            <th>Name</th>
            <th>Title</th>
            <th>Current CTC</th>
            <th>Current Location</th>
            <th>Actions</th>
            </tr>
            <?php foreach($applications as $application){ ?> 
            <tr>
            <td><?php echo $application['first_name']." ".$application['last_name'] ?></td>
            <td><?php echo $application['candidate_title'] ?></td>
            <td><?php echo $application['current_ctc'] ?></td>
            <td><?php echo $application['current_location'] ?></td>
            <td><a href="<?php echo site_url('candidate/resume_download/'.$application['candidate_id']); ?>" >Download resume</a></td>
            <?php }; ?>   
            </tr>
        </table>         
    </main>
     <?php include APPPATH.'views/employer/login_footer.php' ?>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>    
</body>
</html>     