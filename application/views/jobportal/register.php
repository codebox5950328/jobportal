<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
<?php include 'header.php' ?> 
<main>
    <h2 align="center">Sign Up</h2>
    <div class="portal-signup">
    <h2 class="main-head"><u>You Are</u></h2>
    <div>
    <div class="main-items">
    <div class="signup-emp">
         <img class="emp-img" src="<?php echo base_url('images/employer.png'); ?>">
        <br><a class="main-link" href="<?php echo site_url("employer/register") ?>">Employer</a></button>
    </div>
    <div class="signup-cand">
        <img class="cand-img" src="<?php echo base_url('images/candidate.png'); ?>">
        <br><a class="main-link" href="<?php echo site_url("candidate/register") ?>">Candidate</a>
    </div>
     </div>
 </div>
</main>
<br><?php include 'footer.php' ?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>       
<body>
    </html>