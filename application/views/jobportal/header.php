<!DOCTYPE html>
<html>
    <head>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
         <link rel="stylesheet" type="text/css" href = "<?php echo base_url(); ?>css/job_portal.css">
</head>
<body>
    <h2 align="center"><u>Job Portal</u></h2>
<nav class="navbar">
<ul class="navbar-list">
     <li class="navbar-items">
    <a class="nav-link" href="<?php echo site_url("jobportal") ?>">Home</a>
     </li>
     <li class="navbar-items">
       <a class="nav-link" href="<?php echo site_url("jobportal/login") ?>">Log in</a>
    </li>
    <li class="navbar-items">   
    <a class="nav-link" href="<?php echo site_url("jobportal/register") ?>">Sign up</a>
    </li>
    <li class="navbar-items">
   <a class="nav-link" href="<?php echo site_url("jobportal/contact_us") ?>">Contact Us</a>
    </li>
 </ul>
</nav>   
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script> 
</body>
</html>


