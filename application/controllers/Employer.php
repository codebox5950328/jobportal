<?php 


class Employer extends CI_Controller{


    public function __construct() {
        parent::__construct();        
            $this->load->model('Employer_model');         
    }

    public function register(){
        $this->form_validation->set_rules("company", "Company Name", "required|alpha");
        $this->form_validation->set_rules("company_address", "Company Address", "required");
        $this->form_validation->set_rules("first_name", "First Name", "required|alpha");
        $this->form_validation->set_rules("last_name", "Last Name", "required|alpha");
        $this->form_validation->set_rules("gender", "Gender", "required");  
        $this->form_validation->set_rules("contact_number", "Contact Number", "required|numeric");
        $this->form_validation->set_rules("current_location", "Current Location", "required|alpha");
        $this->form_validation->set_rules("email_id", "Email Id", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required|alpha_numeric_spaces");
        $this->form_validation->set_rules("re-enter_password", "Re-enter password", "required|matches[password]");
        $this->form_validation->set_rules('conditions','Terms and Conditions','trim|required|greater_than[0]');

        if($this->form_validation->run()==FALSE){
            $this->load->view("employer/signup");
        }
        else{
            $employer=array(
                'company_name'=> $this->input->post('company'),
                'company_address'=> $this->input->post('company_address'),
                'first_name'=> $this->input->post('first_name'),
                'last_name'=> $this->input->post('last_name'),
                'gender'=> $this->input->post('gender'),
                'contact_number'=> $this->input->post('contact_number'),
                'current_location'=> $this->input->post('current_location'),
                'email_id'=> $this->input->post('email_id'),
                'password'=> $this->input->post('password'),
            );
            $this->Employer_model->add($employer);
            redirect('employer/login');
            exit;
        }
    }

    public function login(){
            $this->form_validation->set_rules("username", "Username", "required|valid_email");
            $this->form_validation->set_rules("password", "Password", "required|alpha_numeric_spaces");

        if($this->form_validation->run()==FALSE){
            $this->load->view("employer/login");
        }
        else{
            $username=$this->input->post("username");
            $password=$this->input->post("password");

            $this->load->model("Employer_model");
            $employer=$this->Employer_model->get_employer($username,$password);
            if($employer){
                $this->session->set_userdata('emp_id',$employer['id']);
                $this->session->set_flashdata("msg","loged in successfully");
                redirect('employer/dashboard');
                exit;
            }
            else{
                $this->session->set_flashdata("msg","Your username or password did not match");
                $this->load->view("employer/login");
            }
        }
    }

    public function dashboard(){
        if(!$this->session->userdata('emp_id')){
            redirect("employer/login");
        }
        else{
            $this->load->view("employer/dashboard");
        }
    }

    public function my_jobs(){
        if(!$this->session->userdata('emp_id')){
            redirect("employer/login");
        }
        else{
            $emp_id=$this->session->userdata('emp_id');
            $this->load->model("Job_model");
            $employer['jobs']=$this->Job_model->get_employer_jobs($emp_id);
            $this->load->view("employer/my_jobs",$employer);
        }
    }

    public function profile(){
        if(!$this->session->userdata('emp_id')){
            redirect("employer/login");
        }
        else{
            $id=$this->session->userdata('emp_id');
            $data['employer']=$this->Employer_model->get_employer_by_id($id);
            $this->load->view('employer/my_profile',$data); 
        } 
    }
    

    public function logout(){
        $this->session->sess_destroy();
        redirect('employer/login');
    }

}