<?php 


class Job extends CI_Controller{

    public function __construct() {
        parent::__construct();        
            $this->load->model('Job_model');         
    }

    public function search(){
        $job_type=$this->input->post('job_type');
        $job_location=$this->input->post('job_location');       
        $data["jobs"]=$this->Job_model->search($job_type,$job_location);
        $this->load->view("job/search",$data);
    }



    public function post(){
        $this->form_validation->set_rules('job_title',"Job Title","required");
        $this->form_validation->set_rules('description',"Description","required");
        $this->form_validation->set_rules('budget_ctc',"Budget CTC","required");
        
        if($this->form_validation->run()==FALSE){
            $this->load->view("job/post");
        }
        else{
            $job=array(
               'emp_id'=>$this->session->userdata('id'),
               'job_title'=>$this->input->post('job_title'),
               'description'=>$this->input->post('description'),
               'budget'=>$this->input->post('budget_ctc'),
               'job_type'=>$this->input->post('job_type'), 
               'job_location'=>$this->input->post('job_location'),
               'min_experience_required'=>$this->input->post('experience_require')
            );
            $this->Job_model->add($job);
            redirect("job/post");
            exit;  
        }
    }


    public function update($id){
        $this->form_validation->set_rules('job_title',"Job Title","required");
        $this->form_validation->set_rules('description',"Description","required");
        $this->form_validation->set_rules('budget_ctc',"Budget CTC","required");
        $this->form_validation->set_rules('experience_require',"Min Experience Required","required");

        if($this->form_validation->run()==FALSE){
            $edit["job"]=$this->Job_model->edit($id);
            $this->load->view("job/update",$edit);
        }
        else{
            $data=array(
                'emp_id'=>$this->session->userdata('id'),
                'job_title'=>$this->input->post('job_title'),
                'description'=>$this->input->post('description'),
                'budget'=>$this->input->post('budget_ctc'),
                'job_type'=>$this->input->post('job_type'), 
                'job_location'=>$this->input->post('job_location'),
                'min_experience_required'=>$this->input->post('experience_require')
            );
            $this->Job_model->update($data,$id);
            redirect("employer/my_jobs");
            exit;
        }
    }

  
    public function delete($id){
        $this->Job_model->delete($id);
        redirect('employer/my_jobs');
        exit;
    }

    public function details($id){
       $data['job']=$this->Job_model->get_job_details($id);
       $this->load->view("job/details",$data);
    }


    public function view_applications($id){
       $data["applications"]=$this->Job_model->view_applications($id);
       $this->load->view('job/view_applications',$data);
    } 
}