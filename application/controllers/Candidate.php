<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Candidate extends CI_Controller{
    
    public function __construct() {
            parent::__construct();        
               $this->load->model('Candidate_model');         
    }

    public function register(){
        $this->form_validation->set_rules("candidate_title", "Job Title","required");
        $this->form_validation->set_rules("current_ctc", "Current CTC","required|numeric");
        $this->form_validation->set_rules("first_name", "First Name", "required");
        $this->form_validation->set_rules("last_name", "Last Name", "required");
        $this->form_validation->set_rules("gender", "Gender", "required");  
        $this->form_validation->set_rules("date_of_birth","Date of Birth", "required");
        $this->form_validation->set_rules("contact_number", "Contact Number", "required|numeric");
        $this->form_validation->set_rules("current_location", "Current Location", "required");
        $this->form_validation->set_rules("email_id", "Email Id", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required");
        $this->form_validation->set_rules("re-enter_password", "Re-enter password", "required|matches[password]");
        $this->form_validation->set_rules('terms','Terms and Conditions','trim|required|greater_than[0]');
        

        if($this->form_validation->run()==FALSE){
            $this->load->view("candidate/signup"); 
        }    
        else{
            $candidate=array(
               'candidate_title'=>$this->input->post('candidate_title'),
               'current_ctc'=> $this->input->post('current_ctc'),
               'first_name'=> $this->input->post('first_name'),
               'last_name'=> $this->input->post('last_name'),
               'gender'=> $this->input->post('gender'),
               'date_of_birth' =>$this->input->post('date_of_birth'),
               'contact_number'=> $this->input->post('contact_number'),
               'current_location'=> $this->input->post('current_location'),
               'email_id'=> $this->input->post('email_id'),
               'password'=> $this->input->post('password'),
            );
            $this->Candidate_model->add($candidate);
            $this->session->set_flashdata("msg","Registered successfully");
            redirect("candidate/login");
        }
    }

    public function login(){
        $this->form_validation->set_rules("username", "Username", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required|alpha_numeric_spaces");

        if($this->form_validation->run()==FALSE){
            $this->load->view("candidate/login");
        }
        else{
            $username=$this->input->post("username");
            $password=$this->input->post("password");
            $this->session->set_userdata('username',$username);
            $this->session->set_userdata('password',$password);
            $candidate=$this->Candidate_model->get_candidate($username,$password);

            if($candidate){
               $this->session->set_userdata('candidate_id',$candidate['id']);
               $this->session->set_flashdata("msg","logged in successfully");
               redirect('candidate/dashboard');
               exit; 
            }
            else{
            $this->session->set_flashdata("msg","Your username or password did not match");
            $this->load->view("candidate/login");
            }
        } 
    }

    public function dashboard(){
        if(!$this->session->userdata('candidate_id')){
            redirect("candidate/login");
        }
        else{
            $this->load->view("candidate/dashboard");
        }
    }


    public function insert_job($id){
        $job_applications=array(
            'job_id'=>$id,
            'candidate_id'=>$this->session->userdata('candidate_id'),
            'emp_id'=>$this->session->userdata('emp_id'),
            'applied_on'=>time()
        );
        $this->Candidate_model->insert_job($job_applications);
        redirect("job/search");
        exit;
    }

    public function applied_jobs(){
        if(!$this->session->userdata('candidate_id')){
            redirect("candidate/login");
        }
        else{
            $id=$this->session->userdata('candidate_id');
            $data['jobs']=$this->Candidate_model->applied_jobs($id);
            $this->load->view("candidate/my_applied_jobs",$data);
        }
    }

    public function profile(){

        if(!$this->session->userdata('candidate_id')){
            redirect("candidate/login");
        }
        else{    
            $id=$this->session->userdata('candidate_id');
            $data['candidate']=$this->Candidate_model->get_candidate_by_id($id);
            $this->load->view("candidate/my_profile",$data);
        }
    }


    public function edit_profile(){

        $this->form_validation->set_rules('exp','Experience','required');
        $this->form_validation->set_rules("candidate_title", "Job Title","required");
        $this->form_validation->set_rules("current_ctc", "Current CTC","required|numeric");
        $this->form_validation->set_rules("first_name", "First Name", "required|alpha");
        $this->form_validation->set_rules("last_name", "Last Name", "required|alpha");
        $this->form_validation->set_rules("gender", "Gender", "required");  
        $this->form_validation->set_rules("date_of_birth","Date of Birth", "required");
        $this->form_validation->set_rules("contact_number", "Contact Number", "required|numeric");
        $this->form_validation->set_rules("current_location", "Current Location", "required|alpha");
        $this->form_validation->set_rules("email_id", "Email Id", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required|alpha_numeric_spaces");
        $this->form_validation->set_rules('terms','Terms and Conditions','trim|required|greater_than[0]');
        
        if(!$this->session->userdata('candidate_id')){
            redirect("candidate/login");
        }  

        if($this->form_validation->run()==FALSE){
            $id=$this->session->userdata('candidate_id');
            $candidate=$this->Candidate_model->edit_profile($id);
            $this->session->set_userdata('resume',$candidate['resume']);
            $this->load->view('candidate/edit_profile',$candidate);
        }
        else{
            $update=array(  
                'experience'=>$this->input->post('exp'),
                'candidate_title'=>$this->input->post('candidate_title'),
                'current_ctc'=> $this->input->post('current_ctc'),
                'first_name'=> $this->input->post('first_name'),
                'last_name'=> $this->input->post('last_name'),
                'gender'=> $this->input->post('gender'),
                'date_of_birth' =>$this->input->post('date_of_birth'),
                'contact_number'=> $this->input->post('contact_number'),
                'current_location'=> $this->input->post('current_location'),
                'email_id'=> $this->input->post('email_id'),
                'password'=> $this->input->post('password'),
            );

            if (!empty($_FILES['resume'])) {
                $file_name=$_FILES['resume']['name'];
                $file_size=$_FILES['resume']['size'];
                $file_tmp=$_FILES['resume']['tmp_name'];
                $file_type=$_FILES['resume']['type'];
                move_uploaded_file($file_tmp, 'uploads/'.$file_name);
                $update['resume']=$file_name;
            }

            $id= $this->session->userdata('candidate_id');
            $this->Candidate_model->update_profile($update,$id);
            redirect("candidate/profile");
            exit;
        }
    }

    public function resume_view($id){
        $resume =$this->session->userdata('resume');                                                                     
        $file = 'uploads/'.$resume;

        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');
        @readfile($file);
    }

    public function resume_download($id){
        $this->load->helper('download');
        $resume =$this->session->userdata('resume');                                                                     
        $file = 'uploads/'.$resume;
        force_download($file,NULL);
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('candidate/login');
    }
}