<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Jobportal extends CI_Controller{

    public function index(){

        $this->load->view('jobportal/home');
    }

    
    public function register(){
     
        $this->load->view('jobportal/register');
    }

    public function login(){
      
       $this->load->view('jobportal/login');
    }

    public function contact_us(){

        $this->load->view('jobportal/contact_us');
    }
}